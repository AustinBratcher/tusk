#!/bin/bash

function vowel() {
    s=aeoiu
    p=$(( $RANDOM % 5))
    echo -n ${s:$p:1}
}


function letter() {
    s=abcdefghijklmnopqrstuvwxyz
    p=$(( $RANDOM % 26))
    echo -n ${s:$p:1}
}

echo `letter; letter; letter; letter; letter; letter; letter; letter;`
