#!/bin/bash

echo "This process will require a system reboot. Do you want to continue:[Y/n]"

read response

#determine if they want to continue:

case $response in
n) exit ;;
N) exit ;;
no) exit ;;
No) exit ;;
NO) exit ;;
esac

#------------------------Setup---------------------------
sudo apt-get install git

#install node
cd /
sudo git clone git://github.com/ry/node.git
cd ./node
./configure
make
sudo make install

#install file server
npm install -g http-server
mkdir ~/ivory
cd ~/ivory

#---------------------------IVORY------------------------

# cp server files to ~/ivory/ivory
git clone git@bitbucket.org:codeslingerscapstone/capstone.git
mv ./capstone ./ivory
cd ./ivory

sudo rm ./.gitignore
sudo rm -r ./.git
npm install

#--------------------------Set up database---------------

#put kevin's script here!!

#---------------------------TUSK-------------------------
cd ~
#create user group called docker
sudo groupadd docker

#install Docker
sudo apt-get update
sudo apt-get install -y docker.io
sudo ln -sf /usr/bin/docker.io /usr/local/bin/docker

#make sure docker deamon is working
#we will assume it is working, but we should give instructions on how 
# to fix certain issues that might come up (i.e. someone tries to start
# docker deamon when it is already running)
sudo docker.io pull arbratcher/tusk


cd ~/ivory


# Copy files from cd or wherever
git clone git@bitbucket.org:Austin_Bratcher/tusk.git
cd ./tusk
sudo rm ./.gitignore
sudo rm -r ./.git #remove .git to remove local repo


mkdir ~/ivory/tusk/programs
mkdir ~/ivory/tusk/user_folders

#enable swap limit on machine
sudo perl -pi -e 's/GRUB_CMDLINE_LINUX=""/GRUB_CMDLINE_LINUX="cgroup_enable=memory swapaccount=1"/g' /etc/default/grub
sudo update-grub
sudo reboot

# make sure to tell them to run tuskStart.sh on boot!
#--> shoudl start:
# IVORY
# TUSK
# HTTP-SERVER (in correct location!)



