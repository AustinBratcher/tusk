//Austin Bratcher
//holyBatman.cpp
//ASSIGNMENT 308

#include <vector>
#include <iostream>
#include <utility> 
#include <cstdlib>
#include <set>

using namespace std;

typedef vector<vector<int> > board;
typedef vector<pair<int,int> > queens;

int findNumWays(board, queens, int, vector<int>);
double slope(pair<int,int>, pair<int,int>);
void printBoard(board);

int main(){

    int boardSize;
    int numHoles;
    
    while((cin >> boardSize >> numHoles) && (boardSize != 0)){
        board current = board(boardSize, vector<int>(boardSize, 1));
        int row;
        int col;
        for(int x = 0; x < numHoles; x++){
            cin >> row >> col;
            current[row][col] = 0;
        }
        queens toPass;
        vector<int> cols;
        for(int x = 0; x < boardSize; x++) cols.push_back(x);
        cout << findNumWays(current, toPass, 0, cols) << endl;
        

    }
    return 0;
}

int findNumWays(board current, queens list, int numQ, vector<int> columns){
    if(numQ == current.size()){
        //printBoard(current);
        return 1;
    }
 
    int toRe = 0;
    for(int x = 0; x < columns.size()-numQ; x++){
        int colVal = columns[x];
        if(current[numQ][colVal] == 1){//checks to see if a whole is in location
            bool canPlace = true;
            
            for(int inner = 0; inner < list.size() && canPlace; inner++){

                if(canPlace && slope(list[inner], make_pair(numQ,colVal))==1.0
                   || slope(list[inner], make_pair(numQ,colVal))==-1.0){
                  canPlace = false;
                } 
            }
            
            if(canPlace){
                current[numQ][colVal] = 2;
                list.push_back(make_pair(numQ,colVal));
                swap(columns[x], columns[columns.size()-1-numQ]);
                toRe += findNumWays(current, list, numQ+1, columns);
                swap(columns[x], columns[columns.size()-1-numQ]);
                list.pop_back();
                current[numQ][colVal] = 1;
            }
        }
    }
   
    return toRe;
}

double slope(pair<int,int> one, pair<int,int> two){
    if(one.second < two.second){
        return ((double)(two.first - one.first))/(two.second - one.second);
    }
    return ((double)(one.first - two.first))/(one.second - two.second);
}

void printBoard(board b){
    for(int x = 0; x < b.size(); x++){
        for(int y = 0; y < b[x].size(); y++){
            cout << b[x][y] << " ";
        }
        cout << endl;
    }
}